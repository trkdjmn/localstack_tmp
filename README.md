# Localstack Tmp


## 始め方

```
docker-compose up -d
docker-compose exec terraform sh -c "terraform init;terraform apply -auto-approve"
```

## リソース確認

* dynamodb,lambda,apigatewayのリソース確認
```
docker-compose exec localstack bash
awslocal lambda list-functions --query "Functions[].[FunctionName]"
awslocal dynamodb list-tables --query "TableNames[]"
awslocal apigateway get-rest-apis  --query "items[].[id,name]"
```
※ 作業端末でaws --endpoint-url=http://localhost:4566でも可

* apigatewayのエンドポイント確認
```
#awslocal apigateway get-rest-apis  --query "items[].[id]" | jq .[][] | xargs -I{} echo http://localhost:4566/restapis/{}/default/_user_request_/
http://localhost:4566/restapis/i5z9t3o544/default/_user_request_/
#
```
## 動作確認

* dynamodbstream
```
# awslocal lambda list-functions --query "Functions[].[FunctionName]" --output text
crevo-dev-rest-api
crevo-dev-stream-processor
crevo-dev-websocket-api
#
# awslocal dynamodb list-tables --query "TableNames[]" --output text
crevo-dev-dynamodb-messages     crevo-dev-dynamodb-threads
#
# awslocal dynamodb put-item \
 --table-name crevo-dev-dynamodb-messages \
 --item '{"ThreadID": { "S": "Thread1" }, "Date":{ "S": "YYYY-MM-DD" }, "UserID":{ "S": "User1" } }' \
 --return-consumed-capacity TOTAL
{
    "ConsumedCapacity": {
        "TableName": "crevo-dev-dynamodb-messages",
        "CapacityUnits": 1.0
    }
}
#
# awslocal logs tail /aws/lambda/crevo-dev-stream-processor
2022-03-16T21:54:06.944000+00:00 2022/03/16/[LATEST]7e1e63a0 Hello from Lambda!
#
 
```

* apigateway
```
# curl -IL http://localhost:4566/restapis/i5z9t3o544/default/_user_request_/test
HTTP/1.1 200
Content-Type: text/html; charset=utf-8
Content-Length: 1773
Access-Control-Allow-Origin: *
Access-Control-Allow-Methods: HEAD,GET,PUT,POST,DELETE,OPTIONS,PATCH
Access-Control-Allow-Headers: authorization,cache-control,content-length,content-md5,content-type,etag,location,x-amz-acl,x-amz-content-sha256,x-amz-date,x-amz-request-id,x-amz-security-token,x-amz-tagging,x-amz-target,x-amz-user-agent,x-amz-version-id,x-amzn-requestid,x-localstack-target,amz-sdk-invocation-id,amz-sdk-request
Access-Control-Expose-Headers: etag,x-amz-version-id
Connection: close
date: Sat, 19 Mar 2022 12:12:12 GMT
server: hypercorn-h11

#
```
