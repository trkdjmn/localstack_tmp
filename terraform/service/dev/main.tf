module "rest_api_lambda" {
  ### Module Path
  source = "../../modules/Lambda"

  lambdafunction_name = "${var.project}-${var.env}-rest-api"
  lambda_exec_role_name = "${var.project}-${var.env}-rest-api-role"
  function_source = "/terraform/functions/rest_api"

  environment_variables = {
    ENV_VAR1 = "environmet1"
    ENV_VAR2 = "environmet2"
  }

  ## output "invoke_arn" {
}

module "websocket_api_lambda" {
  ### Module Path
  source = "../../modules/Lambda"

  lambdafunction_name = "${var.project}-${var.env}-websocket-api"
  lambda_exec_role_name = "${var.project}-${var.env}-websocket-api-role"
  function_source = "/terraform/functions/websocket_api"

  environment_variables = {
    ENV_VAR1 = "environmet1"
    ENV_VAR2 = "environmet2"
  }

  ## output "invoke_arn" {
}

module "stream_processor_lambda" {
  ### Module Path
  source = "../../modules/Lambda"

  lambdafunction_name = "${var.project}-${var.env}-stream-processor"
  lambda_exec_role_name = "${var.project}-${var.env}-stream-processor-role"
  function_source = "/terraform/functions/stream_processor"

  environment_variables = {
    ENV_VAR1 = "environmet1"
    ENV_VAR2 = "environmet2"
  }

  ## output "arn" {
  ## output "invoke_arn" {
}

module "dynamodb_threads" {
  ### Module Path
  source = "../../modules/DynamoDB"

  table_name = "${var.project}-${var.env}-dynamodb-threads"

  hash_key = "ThreadID"
  range_key = "UserID"
  attributes = [
    {
      name = "ThreadID"
      type = "S"
    },
    {
      name = "UserID"
      type = "S"
    }
  ]

  ## output "arn" {
  ## output "stream_arn" {
}

module "dynamodb_messages" {
  ### Module Path
  source = "../../modules/DynamoDB"

  table_name = "${var.project}-${var.env}-dynamodb-messages"

  hash_key = "ThreadID"
  range_key = "Date"
  attributes = [
    {
      name = "ThreadID"
      type = "S"
    },
    {
      name = "Date"
      type = "S"
    }
  ]
  
  ## output "arn" {
  ## output "stream_arn" {
}

resource "aws_lambda_event_source_mapping" "stream" {
  event_source_arn  = module.dynamodb_messages.stream_arn
  function_name     = module.stream_processor_lambda.arn
  starting_position = "LATEST"
}

module "apigateway_restapi" {
  ### Module Path
  source = "../../modules/ApiGatewayv1"

  apigateway_name = "${var.project}-${var.env}-apigw-restapi"
  lambda_arn = module.rest_api_lambda.invoke_arn

}

#resource "aws_lambda_permission" "apigw" {
#  statement_id  = "AllowAPIGatewayInvoke"
#  action        = "lambda:InvokeFunction"
#  function_name = "${var.project}-${var.env}-rest-api"
#  principal     = "apigateway.amazonaws.com"
#
#  source_arn = "${module.apigateway_restapi.execution_arn}/*"
#}
