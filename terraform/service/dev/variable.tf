## IAM Setting
variable "aws_access_key" {
  type    = string
  default = "dummy"
}
variable "aws_secret_key" {
  type    = string
  default = "dummy"
}

## Project Name
variable "project" {
  type    = string
  default = "crevo"
}
variable "env" {
  type    = string
  default = "dev"
}
