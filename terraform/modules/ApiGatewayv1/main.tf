output "execution_arn" {
    value = aws_api_gateway_rest_api.main.execution_arn
}

resource aws_api_gateway_rest_api main {
  name        = var.apigateway_name
  description = "REST API"
}

resource aws_api_gateway_resource proxy {
   rest_api_id = aws_api_gateway_rest_api.main.id
   parent_id   = aws_api_gateway_rest_api.main.root_resource_id
   path_part   = "{proxy+}"
}

resource aws_api_gateway_method proxy {
   rest_api_id   = aws_api_gateway_rest_api.main.id
   resource_id   = aws_api_gateway_resource.proxy.id
   http_method   = "ANY"
   authorization = "NONE"
}

resource aws_api_gateway_integration lambda {
   rest_api_id = aws_api_gateway_rest_api.main.id
   resource_id = aws_api_gateway_method.proxy.resource_id
   http_method = aws_api_gateway_method.proxy.http_method

   integration_http_method = "ANY"
   type                    = "AWS_PROXY"
   uri                     = var.lambda_arn
}

resource aws_api_gateway_method proxy_root {
   rest_api_id   = aws_api_gateway_rest_api.main.id
   resource_id   = aws_api_gateway_rest_api.main.root_resource_id
   http_method   = "ANY"
   authorization = "NONE"
}

resource aws_api_gateway_integration lambda-root {
   rest_api_id = aws_api_gateway_rest_api.main.id
   resource_id = aws_api_gateway_method.proxy_root.resource_id
   http_method = aws_api_gateway_method.proxy_root.http_method

   integration_http_method = "POST"
   type                    = "AWS_PROXY"
   uri                     = var.lambda_arn
}

resource aws_api_gateway_deployment default {
   depends_on = [
     aws_api_gateway_integration.lambda-root
   ]

   rest_api_id = aws_api_gateway_rest_api.main.id
   stage_name  = "default"
}