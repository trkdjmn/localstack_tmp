resource "aws_apigatewayv2_api" "api_http" {
  name          = var.apigateway_name
  protocol_type = "HTTP"
}

resource "aws_apigatewayv2_integration" "api_http" {
  api_id                 = aws_apigatewayv2_api.api_http.id
  integration_type       = "AWS_PROXY"
  connection_type        = "INTERNET"
  integration_method     = "ANY"
  integration_uri        = var.lambda_arn
  payload_format_version = "2.0"
}

resource "aws_apigatewayv2_route" "api_http" {
  api_id    = aws_apigatewayv2_api.api_http.id
  route_key = "$default"
  target    = "integrations/${aws_apigatewayv2_integration.api_http.id}"
}

resource "aws_apigatewayv2_stage" "default" {
  api_id      = aws_apigatewayv2_api.api_http.id
  name        = "default"
  auto_deploy = true
  #access_log_settings {
  #  destination_arn = aws_cloudwatch_log_group.api_http.arn
  #  format          = jsonencode({ "requestId" : "$context.requestId", "ip" : "$context.identity.sourceIp", "requestTime" : "$context.requestTime", "httpMethod" : "$context.httpMethod", "routeKey" : "$context.routeKey", "status" : "$context.status", "protocol" : "$context.protocol", "responseLength" : "$context.responseLength" })
  #}
}

resource "aws_cloudwatch_log_group" "api_http" {
  name              = "/aws/apigateway/${var.apigateway_name}"
  retention_in_days = 7
}

