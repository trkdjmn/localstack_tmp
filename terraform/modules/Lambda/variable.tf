variable "lambda_exec_role_name" {}
variable "lambdafunction_name" {}
variable "function_source" {}
variable "environment_variables" {}
variable "iam_policy_arn" {
  type = list(string)
  default = [
    "arn:aws:iam::aws:policy/AmazonS3FullAccess",
    "arn:aws:iam::aws:policy/CloudWatchFullAccess",
    "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess",
    "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
  ]
}
