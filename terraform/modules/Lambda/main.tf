#resource "aws_iam_role" "lambda_exec_role" {
#  name               = var.lambda_exec_role_name
#  assume_role_policy = <<EOF
#{
#  "Version": "2008-10-17",
#  "Statement": [
#    {
#      "Sid": "",
#      "Effect": "Allow",
#      "Principal": {
#        "Service": [
#          "lambda.amazonaws.com"
#        ]
#      },
#      "Action": "sts:AssumeRole"
#    }
#  ]
#}
#EOF
#}

output "arn" {
    value = aws_lambda_function.lambda_function.arn
}

output "invoke_arn" {
    value = aws_lambda_function.lambda_function.invoke_arn
}

resource "aws_cloudwatch_log_group" "cwlogs" {
  name              = "/aws/lambda/${var.lambdafunction_name}"
  retention_in_days = 0
}

data "archive_file" "function_source" {
  type        = "zip"
  source_dir  = "${var.function_source}"
  output_path = "${path.module}/archive/${var.lambdafunction_name}.zip"
}

resource "aws_lambda_function" "lambda_function" {
  function_name = var.lambdafunction_name
  handler       = "lambda_function.lambda_handler"
  role          = "aws_iam_role.lambda_exec_role.arn"
  runtime       = "ruby2.7"
  timeout       = 600

  filename         = data.archive_file.function_source.output_path
  source_code_hash = data.archive_file.function_source.output_base64sha256

  environment {
    variables = "${var.environment_variables}"
  }
}

